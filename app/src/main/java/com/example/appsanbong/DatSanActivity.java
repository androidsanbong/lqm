package com.example.appsanbong;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class DatSanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dat_san);

        Spinner choGio = findViewById(R.id.chongio);
        Button datsan = findViewById(R.id.btn_dat_san);

        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(DatSanActivity.this,
                android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.time));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item );
        choGio.setAdapter(myAdapter);

        datsan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DatSanActivity.this,HomeActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(DatSanActivity.this, "Đặt sân thành công", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
