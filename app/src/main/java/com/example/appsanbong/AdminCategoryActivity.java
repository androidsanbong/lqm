package com.example.appsanbong;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AdminCategoryActivity extends AppCompatActivity
{
    private TextView tk,hc1,hc2,st,hk,hv,lc,nhs;
    Button logout,quanlysan;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_category);


        tk = (TextView) findViewById(R.id.txtTK);
        hc1 = (TextView) findViewById(R.id.txtHC1);
        hc2 = (TextView) findViewById(R.id.txtHC2);
        hk = (TextView) findViewById(R.id.txtHK);
        nhs = (TextView) findViewById(R.id.txtNHS);
        lc = (TextView) findViewById(R.id.txtLC);
        hv = (TextView) findViewById(R.id.txtHV);
        st = (TextView) findViewById(R.id.txtST);
        logout = (Button) findViewById(R.id.btn_logout_admin) ;
        quanlysan = (Button) findViewById(R.id.btn_qlsan_admin) ;

        tk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminAddNewProductActivity.class);
                intent.putExtra("khu vực", "Thanh Khê");
                startActivity(intent);
            }
        });


        hc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminAddNewProductActivity.class);
                intent.putExtra("khu vực", "Hải Châu 1");
                startActivity(intent);
            }
        });


        hc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminAddNewProductActivity.class);
                intent.putExtra("khu vực", "Hải Châu 2");
                startActivity(intent);
            }
        });


        hk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminAddNewProductActivity.class);
                intent.putExtra("khu vực", "Hòa Khánh");
                startActivity(intent);
            }
        });


        nhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminAddNewProductActivity.class);
                intent.putExtra("khu vực", "Ngũ Hành Sơn");
                startActivity(intent);
            }
        });


        hv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminAddNewProductActivity.class);
                intent.putExtra("khu vực", "Hòa Vang");
                startActivity(intent);
            }
        });



        lc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminAddNewProductActivity.class);
                intent.putExtra("khu vực", "Liên Chiểu");
                startActivity(intent);
            }
        });



        st.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminAddNewProductActivity.class);
                intent.putExtra("khu vực", "Sơn Trà");
                startActivity(intent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        quanlysan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,HomeActivity.class);
                intent.putExtra("Admin","Admin");
                startActivity(intent);
            }
        });
    }
}
