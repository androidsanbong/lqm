package com.example.appsanbong.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

    import com.example.appsanbong.Interface.ItemClickListner;
import com.example.appsanbong.R;

import org.w3c.dom.Text;

public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView txttensan, txtchitietsan, txtdiachi;
    public ImageView imageView;
    public ItemClickListner listner;


    public ProductViewHolder(View itemView)
    {
        super(itemView);


        imageView = (ImageView) itemView.findViewById(R.id.san_image);
        txttensan = (TextView) itemView.findViewById(R.id.ten_san);
        txtchitietsan = (TextView) itemView.findViewById(R.id.chi_tiet_san);
        txtdiachi = (TextView) itemView.findViewById(R.id.dia_chi);
    }

    public void setItemClickListner(ItemClickListner listner)
    {
        this.listner = listner;
    }

    @Override
    public void onClick(View view)
    {
        listner.onClick(view, getAdapterPosition(), false);
    }
}
