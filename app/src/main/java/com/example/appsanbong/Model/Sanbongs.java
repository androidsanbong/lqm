package com.example.appsanbong.Model;

public class Sanbongs {
    private String tensan, description, diachi, image, khuvuc, sanid, date, time;

    public Sanbongs() {

    }

    public Sanbongs(String tensan, String description, String diachi, String image, String khuvuc, String sanid, String date, String time) {
        this.tensan = tensan;
        this.description = description;
        this.diachi = diachi;
        this.image = image;
        this.khuvuc = khuvuc;
        this.sanid = sanid;
        this.date = date;
        this.time = time;
    }

    public String getTensan() {
        return tensan;
    }

    public void setTensan(String tensan) {
        this.tensan = tensan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getKhuvuc() {
        return khuvuc;
    }

    public void setKhuvuc(String khuvuc) {
        this.khuvuc = khuvuc;
    }

    public String getSanid() {
        return sanid;
    }

    public void setSanid(String sanid) {
        this.sanid = sanid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}