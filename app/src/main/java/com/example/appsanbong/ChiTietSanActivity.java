package com.example.appsanbong;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.appsanbong.Model.Sanbongs;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ChiTietSanActivity extends AppCompatActivity {

    private ImageView sanImage;
    private TextView tensan,diachi,chitiet;
    private Button datsan;
    private String Idsan = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_san);

        Idsan = getIntent().getStringExtra("sanid");

        sanImage = (ImageView) findViewById(R.id.image_san);
        tensan = (TextView) findViewById(R.id.txt_tensan);
        chitiet = (TextView) findViewById(R.id.txt_chitiet);
        diachi = (TextView) findViewById(R.id.txt_diachi);
        datsan = (Button) findViewById(R.id.btn_datsan);


        getsanbongDetails(Idsan);
    }

    private void getsanbongDetails(String idsan)
    {
        final DatabaseReference sanbongsRef = FirebaseDatabase.getInstance().getReference().child("Sanbongs");

        sanbongsRef.child(idsan).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    Sanbongs sanbongs = dataSnapshot.getValue(Sanbongs.class);

                    tensan.setText(sanbongs.getTensan());
                    diachi.setText(sanbongs.getDiachi());
                    chitiet.setText(sanbongs.getDescription());
                    Picasso.get().load(sanbongs.getImage()).into(sanImage);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        datsan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChiTietSanActivity.this,DatSanActivity.class);
                startActivity(intent);
            }
        });
    }
}
