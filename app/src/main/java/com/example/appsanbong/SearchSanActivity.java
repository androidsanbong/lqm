package com.example.appsanbong;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.appsanbong.Model.Sanbongs;
import com.example.appsanbong.ViewHolder.ProductViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.lang.ref.Reference;

public class SearchSanActivity extends AppCompatActivity {
    Button searchBtn;
    EditText inputTxt;
    RecyclerView searchList;
    String SearchInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_san);

        searchBtn = findViewById(R.id.search_btn);
        inputTxt = findViewById(R.id.Search_san_edt);
        searchList = findViewById(R.id.search_list);
        searchList.setLayoutManager(new LinearLayoutManager(SearchSanActivity.this));

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchInput = inputTxt.getText().toString();

                onStart();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Sanbongs");


        FirebaseRecyclerOptions<Sanbongs> options = new FirebaseRecyclerOptions.Builder<Sanbongs>()
                .setQuery(reference.orderByChild("tensan").startAt(SearchInput),Sanbongs.class).build();

        FirebaseRecyclerAdapter<Sanbongs, ProductViewHolder> adapter = new FirebaseRecyclerAdapter<Sanbongs, ProductViewHolder>(options)
        {
            @Override
            protected void onBindViewHolder(@NonNull ProductViewHolder holder, int position, @NonNull final Sanbongs model)
            {
                holder.txttensan.setText(model.getTensan());
                holder.txtchitietsan.setText(model.getDescription());
                holder.txtdiachi.setText("Địa Chỉ:" + model.getDiachi());
                Picasso.get().load(model.getImage()).into(holder.imageView);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(SearchSanActivity.this,ChiTietSanActivity.class);
                        intent.putExtra("sanid",model.getSanid());
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_items_layout, parent, false);
                ProductViewHolder holder = new ProductViewHolder(view);
                return holder;
            }
        };
        searchList.setAdapter(adapter);
        adapter.startListening();
    }
}
