package com.example.appsanbong;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class QuanLySanActivity extends AppCompatActivity {

    Button capnhat,delete;
    EditText tensan,diachi,chitiet;
    ImageView imageView;

    private String Idsan = "";
    private DatabaseReference sanbongsRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_san);

        Idsan = getIntent().getStringExtra("sanid");
        sanbongsRef = FirebaseDatabase.getInstance().getReference().child("Sanbongs").child(Idsan);

        capnhat = findViewById(R.id.btn_apply);
        tensan = findViewById(R.id.ten_san_admin);
        diachi = findViewById(R.id.dia_chi_admin);
        chitiet = findViewById(R.id.chi_tiet_san_admin);
        imageView = findViewById(R.id.san_image_admin);
        delete = findViewById(R.id.btn_delete);



        displaySpecificSanbongInfo();

        capnhat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                capnhatsan();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xoasanbong();
            }
        });
    }

    private void xoasanbong()
    {
        sanbongsRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                Intent intent = new Intent(QuanLySanActivity.this,AdminCategoryActivity.class);
                startActivity(intent);
                finish();

                Toast.makeText(QuanLySanActivity.this, "Xóa sân thàng công !", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void capnhatsan()
    {
        String sTensan = tensan.getText().toString();
        String sDiachi = diachi.getText().toString();
        String sChitet = chitiet.getText().toString();

        if (sTensan.equals(""))
        {
            Toast.makeText(this, "Không được bỏ trống !", Toast.LENGTH_SHORT).show();
        }
        else if (sDiachi.equals(""))
        {
            Toast.makeText(this, "Không được bỏ trống !", Toast.LENGTH_SHORT).show();
        }
        else if (sChitet.equals(""))
        {
            Toast.makeText(this, "Không được bỏ trống !", Toast.LENGTH_SHORT).show();
        }
        else
        {
            HashMap<String, Object> productMap = new HashMap<>();
            productMap.put("sanid", Idsan);
            productMap.put("description",sChitet);
            productMap.put("diachi", sDiachi);
            productMap.put("tensan", sTensan);

            sanbongsRef.updateChildren(productMap).addOnCompleteListener(new OnCompleteListener<Void>()
            {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful())
                    {
                        Toast.makeText(QuanLySanActivity.this, "Cập nhật thành công !", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(QuanLySanActivity.this,AdminCategoryActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            });
        }

    }



    private void displaySpecificSanbongInfo()
    {
        sanbongsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if (dataSnapshot.exists())
                {
                    String stensan = dataSnapshot.child("tensan").getValue().toString();
                    String sdiachi = dataSnapshot.child("diachi").getValue().toString();
                    String schitiet = dataSnapshot.child("description").getValue().toString();
                    String image = dataSnapshot.child("image").getValue().toString();

                    tensan.setText(stensan);
                    diachi.setText(sdiachi);
                    chitiet.setText(schitiet);
                    Picasso.get().load(image).into(imageView);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
